# OpenML dataset: Electric_Vehicles

https://www.openml.org/d/45948

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Description:
The Electric_Vehicle_Population_Data.csv dataset provides a comprehensive overview of the electric vehicle (EV) population within a specific region, highlighting key information about the vehicles, their owners, and associated geographical details. It encompasses a variety of attributes including Vehicle Identification Number (VIN), county and city of the owner, state, postal code, model year, make and model of the vehicle, type of electric vehicle (battery electric vehicle (BEV) or plug-in hybrid electric vehicle (PHEV)), eligibility for clean alternative fuel vehicle (CAFV) programs, electric range, base Manufacturer Suggested Retail Price (MSRP), legislative district of the vehicle location, Department of Licensing (DOL) vehicle ID, precise vehicle location coordinates, the electric utility provider, and the 2020 census tract. This dataset serves as a vital resource for understanding the distribution, diversity, and adoption trends of electric vehicles in specified areas, aiding in infrastructural planning, environmental research, and policy making aimed at promoting the use of clean alternative fuel vehicles.

Attribute Description:
- VIN: Unique vehicle identification number.
- County/City/State/Postal Code: Geographical information of the vehicle owner.
- Model Year: Year the vehicle was manufactured.
- Make/Model: The brand and model of the vehicle.
- Electric Vehicle Type: Specifies if the vehicle is a BEV or PHEV.
- CAFV Eligibility: Indicates if the vehicle is eligible for clean alternative fuel vehicle programs.
- Electric Range: Maximum distance the vehicle can travel on electric power alone.
- Base MSRP: Manufacturer's suggested retail price excluding extras.
- Legislative District: Legislative district where the vehicle is registered.
- DOL Vehicle ID: Unique ID assigned by the Department of Licensing.
- Vehicle Location: Coordinates of the vehicle's registered location.
- Electric Utility: The electric utility provider for the vehicle location.
- 2020 Census Tract: The census tract where the vehicle is registered.

Use Case:
This dataset can be particularly useful for governmental and non-governmental organizations focusing on environmental policy, urban planning, and sustainable transportation. Analysts can use this data to measure the adoption rates of electric vehicles, assess the effectiveness of CAFV incentives, and understand the geographical distribution of EVs. Urban planners can also leverage this information to optimize the placement of charging stations and to forecast the needs for electrical grid upgrades in areas with high EV concentration. Additionally, it provides a foundation for academic research into factors influencing EV adoption and the environmental impact of transitioning to electric vehicles.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/45948) of an [OpenML dataset](https://www.openml.org/d/45948). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/45948/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/45948/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/45948/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

